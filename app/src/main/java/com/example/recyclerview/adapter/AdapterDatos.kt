package com.example.recyclerview.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerview.R
import com.example.recyclerview.model.Afirmacion

class AdapterDatos(
    private val context: Context,
    private val dataset: List<Afirmacion>
    ): RecyclerView.Adapter<AdapterDatos.ItemViewHolder>() {
        class ItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
            val textView1: TextView = view.findViewById(R.id.idHora)
            val textView2: TextView = view.findViewById(R.id.idUbicacion)
            val textView3: TextView = view.findViewById(R.id.Lugar)
            val buttonIr: Button = view.findViewById(R.id.BtnIr)
        }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list, parent, false)
        return ItemViewHolder(adapterLayout)
    }
    override fun getItemCount(): Int {
        return dataset.size
    }
    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = dataset[position]
        holder.textView1.text =  context.resources.getString(item.stringResource1)
        holder.textView2.text =  context.resources.getString(item.stringResource2)
        holder.textView3.text =  context.resources.getString(item.stringResource3)
        holder.buttonIr.setOnClickListener { view ->
            val Titulo = holder.textView3.text
            val IntentUbi = Uri.parse("geo:0,0?q=$Titulo")

            val mapIntent = Intent(Intent.ACTION_VIEW, IntentUbi)
            mapIntent.setPackage("com.google.android.apps.maps")
            context.startActivity(mapIntent)
        }
    }
}

