package com.example.recyclerview.data

import com.example.recyclerview.R
import com.example.recyclerview.model.Afirmacion

class DataSource {
    fun loadAffirmations(): List<Afirmacion> {
        return listOf<Afirmacion>(
            Afirmacion(R.string.affirmation1,R.string.affirmation5,R.string.affirmation13),
            Afirmacion(R.string.affirmation2,R.string.affirmation6,R.string.affirmation14),
            Afirmacion(R.string.affirmation3,R.string.affirmation7,R.string.affirmation15),
            Afirmacion(R.string.affirmation4,R.string.affirmation8,R.string.affirmation16),
            Afirmacion(R.string.affirmation1,R.string.affirmation9,R.string.affirmation17),
            Afirmacion(R.string.affirmation2,R.string.affirmation10,R.string.affirmation18),
            Afirmacion(R.string.affirmation3,R.string.affirmation11,R.string.affirmation19),
            Afirmacion(R.string.affirmation4,R.string.affirmation12,R.string.affirmation20),
        )
    }
}